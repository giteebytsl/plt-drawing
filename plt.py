
import matplotlib.pyplot as plt

def pltShow(x,y):
    # 绘制 Sigmoid 函数图像
    # plt.figure(figsize=(8, 6))
    plt.plot(x, y, label='Sigmoid', color='blue')
    plt.title('函数')
    plt.xlabel('x')
    plt.ylabel('f(x)')
    # plt.axhline(y=0.5, color='r', linestyle='--', label='y = 0.5')
    plt.legend()
    plt.grid(True)
    plt.show()

def readRTK(rtk_file):
    line_x = []
    line_y = []
    x0,y0 = 0,0

    with open(rtk_file,"r") as f:
        lines = f.readlines()

        for num,line in enumerate(lines):
            status,x, y = line.rstrip().split(", ")

            status,x,y = list(map(float,[status,x,y]))
            x = float(x)*10000
            y = float(y)*10000

            if(num == 0):
                x0 = x
                y0 = y

            print("status,x,y -> {},{:.0f},{:.0f},{:.0f}".format(num,status, x-x0 , y-y0  ))
            line_x.append(x-x0)
            line_y.append(y-y0)

    pltShow(line_x, line_y)

def main():
    rtk_file = "../../rtk.txt"
    readRTK(rtk_file)


if __name__ == '__main__':
         main()
